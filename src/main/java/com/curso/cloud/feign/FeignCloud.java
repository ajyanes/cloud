package com.curso.cloud.feign;

import com.curso.cloud.model.dto.CloudDto;
import com.curso.cloud.model.dto.ResponseCloudDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "cloud-service", url = "http://qm2wg.mocklab.io")
public interface FeignCloud {

  @PostMapping("/validate-doctor")
  ResponseCloudDto validateDoctor(@RequestBody final CloudDto cloudDto);

}
