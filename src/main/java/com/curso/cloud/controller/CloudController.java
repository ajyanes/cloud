package com.curso.cloud.controller;

import com.curso.cloud.model.dto.CloudDto;
import com.curso.cloud.model.dto.ResponseCloudDto;
import com.curso.cloud.service.ICloudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/validate")
public class CloudController {

  @Autowired
  ICloudService iCloudService;

  @PostMapping("/doctor")
  public ResponseCloudDto validateDoctor(@RequestBody final CloudDto cloudDto ){

    return iCloudService.validate(cloudDto);
  }

}
