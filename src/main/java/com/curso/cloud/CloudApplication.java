package com.curso.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class CloudApplication {

  public static void main(String[] args) {
    SpringApplication.run(CloudApplication.class, args);
  }

}
