package com.curso.cloud.service;

import com.curso.cloud.model.dto.CloudDto;
import com.curso.cloud.model.dto.ResponseCloudDto;

public interface ICloudService {

  ResponseCloudDto validate(CloudDto cloudDto);

}
