package com.curso.cloud.service.impl;

import com.curso.cloud.feign.FeignCloud;
import com.curso.cloud.model.dto.CloudDto;
import com.curso.cloud.model.dto.ResponseCloudDto;
import com.curso.cloud.service.ICloudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultCloudService implements ICloudService {

  @Autowired
  FeignCloud feignCloud;

  @Override
  public ResponseCloudDto validate(CloudDto cloudDto) {
    return feignCloud.validateDoctor(cloudDto);
  }

}
